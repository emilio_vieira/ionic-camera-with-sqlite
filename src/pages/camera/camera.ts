import { Component } from '@angular/core';
import {  IonicPage, 
          NavController, 
          NavParams, 
          ActionSheetController,
          Platform,
          normalizeURL
        } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FilePath } from '@ionic-native/file-path';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

// page list photos
import { CameraListPage } from "../camera-list/camera-list";

import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';

/**
 * Generated class for the CameraPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-camera',
  templateUrl: 'camera.html',
})
export class CameraPage {

  photo: any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public actionCtrl: ActionSheetController,
    private camera: Camera,
    private platform: Platform,
    private filePath: FilePath,
    private sqlite: SQLite,
    private sanitizer: DomSanitizer

  ) {
  }

  ngOnInit(){
    // creating database and table photos
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    }).then( (db: SQLiteObject) => {  // Same that "function(SQLiteObject){}"

      // create table photos
      db.executeSql('CREATE TABLE photos(url VARCHAR(250))', {})
        .then( ()=> {
          console.log('CREATE TABLE photos');
        })
        .catch((e)=>{

          if(e.code === 5){
            console.log('TABLE "photos" EXISTS!');
          } else {
            console.log('ERROR');
          }

        })
    })
  }



  ionViewDidLoad() {
    console.log('ionViewDidLoad CameraPage');
  }

  choosePhoto() {
    
    // ActionSheet
    let actionSheet = this.actionCtrl.create({
      title: 'Selecione uma imagem',
      buttons: [
        {
          text: 'Tirar foto',
          // role: '',
          handler: () => {
            this.takePhoto();
          }
        },
        {
          text: 'Escolher foto',
          // role: '',
          handler: () => {
            this.takePhoto(this.camera.MediaType.PICTURE,this.camera.DestinationType.NATIVE_URI);
            // this.takePhoto(0,2);
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel'
        }
      ]
    });

    actionSheet.present();

  }

  saveImage() {

    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    })
      .then( (db: SQLiteObject) => {  // Same that "function(SQLiteObject){}"
          // create table photos
          console.log('insert into photos (url) values ("'+this.photo+'")');
          return db.executeSql('insert into photos (url) values ("'+this.photo+'")', {});
      })
      .then( ()=> {
          console.log('this.photo inserted');
          this.navCtrl.push(CameraListPage);
      })
      .catch( (e) => {
          console.log(e);
      });
        
  }

  private takePhoto(source: number = 1, mediaType: number = 0){

    // console.log(this.platform.is('cordova'));
    // if(this.platform.is('cordova')){
      // console.log('device');

      // Options CAMERA
      const options: CameraOptions = {
        quality: 100,
        mediaType: mediaType, // this.camera.MediaType.PICTURE == 1
        sourceType: source,   // this.camera.PictureSourceType.CAMERA == 1
        destinationType: this.camera.DestinationType.FILE_URI,
        encodingType: this.camera.EncodingType.JPEG,
      };

      // take photo CAMERA plugin
      this.camera.getPicture(options).then( (imageData) => {

        var androidGaleria = (source==0 && this.platform.is('android'));

        // if(androidGaleria)
        //   console.log('GALERIA',imageData);
        // else
        //   console.log('tirar foto', imageData);


        if(androidGaleria) {
          
          // only android, resolve PATH
          this.filePath.resolveNativePath(imageData)
            .then( (filePath) => {
              this.photo = filePath;
              console.log(filePath)
            })
            .catch(err => console.log(err))

        } else {

          //let cdvfile = 'cdvfile:/' + imageData.replace(/^file:\/\//,'');
          // console.log('cdvFileURI: ' + cdvfile);
          

          if(this.platform.is('browser')){
            this.photo = this.sanitizer.bypassSecurityTrustResourceUrl(imageData);
            this.photo = 'data:image/jpeg;base64,' + imageData;
          } else {
            //this.photo = this.sanitizer.bypassSecurityTrustResourceUrl(imageData);
            this.photo = normalizeURL(imageData);
          }


        }
  
      }, (err) => {
  
        console.log('ERRO AO TIRAR FOTO',err);
  
      });


    // } else {
      // console.log('browser');
    // }
  }

}