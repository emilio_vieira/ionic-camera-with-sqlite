var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, Platform } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { FilePath } from '@ionic-native/file-path';
import { SQLite } from '@ionic-native/sqlite';
// page list photos
import { CameraListPage } from "../camera-list/camera-list";
import { DomSanitizer } from '@angular/platform-browser';
/**
 * Generated class for the CameraPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CameraPage = /** @class */ (function () {
    function CameraPage(navCtrl, navParams, actionCtrl, camera, platform, filePath, sqlite, sanitizer) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.actionCtrl = actionCtrl;
        this.camera = camera;
        this.platform = platform;
        this.filePath = filePath;
        this.sqlite = sqlite;
        this.sanitizer = sanitizer;
    }
    CameraPage.prototype.ngOnInit = function () {
        // creating database and table photos
        this.sqlite.create({
            name: 'data.db',
            location: 'default'
        }).then(function (db) {
            // create table photos
            db.executeSql('CREATE TABLE photos(url VARCHAR(250))', {})
                .then(function () {
                console.log('CREATE TABLE photos');
            })
                .catch(function (e) {
                if (e.code === 5) {
                    console.log('TABLE "photos" EXISTS!');
                }
                else {
                    console.log('ERROR');
                }
            });
        });
    };
    CameraPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CameraPage');
    };
    CameraPage.prototype.choosePhoto = function () {
        var _this = this;
        // ActionSheet
        var actionSheet = this.actionCtrl.create({
            title: 'Selecione uma imagem',
            buttons: [
                {
                    text: 'Tirar foto',
                    // role: '',
                    handler: function () {
                        _this.takePhoto();
                    }
                },
                {
                    text: 'Escolher foto',
                    // role: '',
                    handler: function () {
                        _this.takePhoto(_this.camera.MediaType.PICTURE, _this.camera.DestinationType.NATIVE_URI);
                        // this.takePhoto(0,2);
                    }
                },
                {
                    text: 'Cancelar',
                    role: 'cancel'
                }
            ]
        });
        actionSheet.present();
    };
    CameraPage.prototype.saveImage = function () {
        var _this = this;
        console.log(this.photo);
        this.sqlite.create({
            name: 'data.db',
            location: 'default'
        })
            .then(function (db) {
            // create table photos
            return db.executeSql('insert into photos (url) values ("' + _this.photo + '")', {});
        })
            .then(function () {
            console.log('this.photo inserted');
            _this.navCtrl.push(CameraListPage);
        })
            .catch(function (e) {
            console.log(e);
        });
    };
    CameraPage.prototype.takePhoto = function (source, mediaType) {
        // console.log(this.platform.is('cordova'));
        // if(this.platform.is('cordova')){
        // console.log('device');
        var _this = this;
        if (source === void 0) { source = 1; }
        if (mediaType === void 0) { mediaType = 0; }
        // Options CAMERA
        var options = {
            quality: 100,
            mediaType: mediaType,
            sourceType: source,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
        };
        // take photo CAMERA plugin
        this.camera.getPicture(options).then(function (imageData) {
            var androidGaleria = (source == 0 && _this.platform.is('android'));
            if (androidGaleria)
                console.log('GALERIA', imageData);
            else
                console.log('tirar foto', imageData);
            if (source == 0 && _this.platform.is('android')) {
                // only android, resolve PATH
                _this.filePath.resolveNativePath(imageData)
                    .then(function (filePath) {
                    _this.photo = filePath;
                    console.log(filePath);
                })
                    .catch(function (err) { return console.log(err); });
            }
            else {
                // this.photo = 'data:image/jpeg;base64,' + imageData;
                var cdvfile = 'cdvfile:/' + imageData.replace(/^file:\/\//, '');
                console.log('imageDataURI: ' + imageData);
                console.log('cdvFileURI: ' + cdvfile);
                _this.photo = _this.sanitizer.bypassSecurityTrustResourceUrl(imageData);
            }
        }, function (err) {
            console.log(err);
        });
        // } else {
        // console.log('browser');
        // }
    };
    CameraPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-camera',
            templateUrl: 'camera.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            ActionSheetController,
            Camera,
            Platform,
            FilePath,
            SQLite,
            DomSanitizer])
    ], CameraPage);
    return CameraPage;
}());
export { CameraPage };
//# sourceMappingURL=camera.js.map